import React, { Component } from 'react';
import Nav from '../Nav/Nav';
import Home from '../Home/Home';
import Content from '../Content/Content';
import Login from '../Login/Login';
import RouterURL from '../RouterURL/RouterURL';

class App extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <RouterURL></RouterURL>
        </div>
      </div>
    );
  }
}

export default App;
