import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from '../Login/Login';
import Home from '../Home/Home';
class RouterURL extends Component {
    render() {
        return (
           <Router>
               <div>
                   <Route exact path="/" component={Login}/>
                   <Route path="/home" component={Home}/>
                </div>
           </Router>
        );
    }
}

export default RouterURL;