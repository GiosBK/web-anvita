import React, { Component } from 'react';
import UserTable from "../UserTable"


class Content extends Component {
  render() {
    return (
      <div className="row">
        <div className="col-xs-12 col-md-6 col-lg-3">
          <div className="panel panel-blue panel-widget ">
            <div className="row no-padding">
              <div className="col-sm-3 col-lg-5 widget-left">
                <svg className="glyph stroked bag"><use xlinkHref="#stroked-bag" /></svg>
              </div>
              <div className="col-sm-9 col-lg-7 widget-right">
                <div className="large">120</div>
                <div className="text-muted">Luật</div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-md-6 col-lg-3">
          <div className="panel panel-teal panel-widget">
            <div className="row no-padding">
              <div className="col-sm-3 col-lg-5 widget-left">
                <svg className="glyph stroked male-user"><use xlinkHref="#stroked-male-user" /></svg>
              </div>
              <div className="col-sm-9 col-lg-7 widget-right">
                <div className="large">24</div>
                <div className="text-muted">Thành viên</div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-md-6 col-lg-3">
          <div className="panel panel-red panel-widget">
            <div className="row no-padding">
              <div className="col-sm-3 col-lg-5 widget-left">
                <svg className="glyph stroked app-window-with-content"><use xlinkHref="#stroked-app-window-with-content" /></svg>
              </div>
              <div className="col-sm-9 col-lg-7 widget-right">
                <div className="large">25.2k</div>
                <div className="text-muted">Lượt tra cứu</div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <UserTable />
        </div>
      </div >
    );
  }
}

export default Content;