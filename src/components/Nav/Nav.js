import React, { Component } from 'react';

class Nav extends Component {
    render() {
        return (
            <nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div className="container-fluid">
                    <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                    </button>
                    <a className="navbar-brand" href="#"><span>QUẢN TRỊ LUẬT</span></a>
                    <ul className="user-menu">
                        <li className="dropdown pull-right">
                        <a href="#" className="dropdown-toggle" data-toggle="dropdown"><svg className="glyph stroked male-user"><use xlinkHref="#stroked-male-user" /></svg><span style={{color: 'white'}}>Xin chào, Thành viên</span> <span className="caret" /></a>
                        <ul className="dropdown-menu" role="menu">
                            <li><a href="#"><svg className="glyph stroked male-user"><use xlinkHref="#stroked-male-user" /></svg> Thông tin thành viên</a></li>
                            <li><a href="#"><svg className="glyph stroked gear"><use xlinkHref="#stroked-gear" /></svg> Cài đặt</a></li>
                            <li><a href="#"><svg className="glyph stroked cancel"><use xlinkHref="#stroked-cancel" /></svg> Đăng xuất</a></li>
                        </ul>
                        </li>
                    </ul>
                    </div>
                </div>{/* /.container-fluid */}
            </nav>
        );
    }
}

export default Nav;