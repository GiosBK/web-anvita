import React, { Component } from 'react';
import PropTypes from 'prop-types';

class UserTable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: [{
                
            }]
        }
    }

    componentDidMount() {
        fetch("")
            .then(response => response.json())
            .then(data => {
                this.setState(prevState => ({
                    users: data.results
                }))
            });
    }


    render() {
        return (
            <table className="table table-border">
                <tr>
                    <th>Name</th>
                    <th>Phone</th>
                </tr>
                <tbody>
                    {
                        this.state.users.map((user, idx) => (
                            <tr>
                                <td>{user.name.first + " " + user.name.last}</td>
                                <td>{user.phone}</td>
                            </tr>))
                    }
                </tbody>
            </table>
        );
    }
}

UserTable.propTypes = {

};

const mapStateToProps = state => ({
    
})

export default UserTable;