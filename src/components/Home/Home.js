import React, { Component } from 'react';
import Nav from '../Nav/Nav';
import Content from '../Content/Content';
import UserTable from "../UserTable"

class Home extends Component {
  render() {
    return (
      <div>
        <Nav></Nav>
        <div id="sidebar-collapse" className="col-sm-3 col-lg-2 sidebar">
          <form role="search">
            <div className="form-group">
              <input type="text" className="form-control" placeholder="Tìm kiếm" />
            </div>
          </form>
          <ul className="nav menu">
            <li className="active"><a href="#"><svg className="glyph stroked dashboard-dial"><use xlinkHref="#stroked-dashboard-dial" /></svg> Trang chủ quản trị Luật</a></li>
            <li className="parent ">
              <a href="#">
                <span data-toggle="collapse" href="#sub-item-1"><svg className="glyph stroked chevron-down"><use xlinkHref="#stroked-chevron-down" /></svg></span> Luật xe máy
                    </a>
              <ul className="children collapse" id="sub-item-1">
                <li>
                  <a href="#">
                    <svg className="glyph stroked plus sign"><use xlinkHref="#stroked-plus-sign" /></svg>
                    Thêm mới
                        </a>
                </li>
              </ul>
            </li>
            <li className="parent ">
              <a href="#">
                <span data-toggle="collapse" href="#sub-item-2"><svg className="glyph stroked chevron-down"><use xlinkHref="#stroked-chevron-down" /></svg></span> Luật Ô tô
                    </a>
              <ul className="children collapse" id="sub-item-2">
                <li>
                  <a className href="#">
                    <svg className="glyph stroked plus sign"><use xlinkHref="#stroked-plus-sign" /></svg> Thêm mới
                        </a>
                </li>
              </ul>
            </li>
            <li className="parent ">
              <a href="#">
                <span data-toggle="collapse" href="#sub-item-3"><svg className="glyph stroked chevron-down"><use xlinkHref="#stroked-chevron-down" /></svg></span> Luật hàng không
                    </a>
              <ul className="children collapse" id="sub-item-3">
                <li>
                  <a className href="#">
                    <svg className="glyph stroked plus sign"><use xlinkHref="#stroked-plus-sign" /></svg> Luật đường thủy
                        </a>
                </li>
              </ul>
            </li>
            <li><a href="#"><svg className="glyph stroked gear"><use xlinkHref="#stroked-gear" /></svg> Cấu hình</a></li>
            <li role="presentation" className="divider" />
            <li><a href="#"><svg className="glyph stroked male-user"><use xlinkHref="#stroked-male-user" /></svg> Đăng xuất</a></li>
          </ul>
        </div>
        <Content>
        </Content>
      </div>
    );
  }
}

export default Home;