import React, { Component } from 'react';

class Login extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
                    <div className="login-panel panel panel-default">
                    <div className="panel-heading">Đăng nhập hệ thống quản trị luật</div>
                    <div className="panel-body">
                        <form method="post" role="form">
                        <fieldset>
                            <div className="form-group">
                            <input className="form-control" placeholder="Tài khoản E-mail" name="email" type="email" autofocus required />
                            </div>
                            <div className="form-group">
                            <input className="form-control" placeholder="Mật khẩu" name="mk" type="password" required />
                            </div>
                            <div className="checkbox">
                            <label>
                                <input name="check" type="checkbox" defaultValue="Remember Me" />Ghi nhớ
                            </label>
                            </div>
                            <br />
                            <input type="submit" name="submit" defaultValue="Đăng nhập" className="btn btn_lg btn-primary" />
                            <input type="reset" name="resset" defaultValue="Làm mới" className="btn btn-primary" />							
                        </fieldset>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;